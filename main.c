#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>     //for unix standard functions
#include <sys/types.h>
#include <time.h>   //header file for time manipulation
#include <sys/wait.h>

//function prototype to get current time
void getCurrentTime();
//function prototype to cast integer to string
char* intToString(int id);
//function prototype to confirm exit
void confirmExit(int number);

int main() {
    //set the number of processes: in our case 10
    int processes_count = 10;
    //declare file descriptors for the parent process
    int parent_file_descriptor[2 * processes_count];
    //declare file descriptors for the child process
    int child_file_descriptor[2 * processes_count];
    //declare the processs to be spawned: in our case, 10
    pid_t child_processes[processes_count];
    //initialize process id for the parent process
    pid_t parent_process = getppid();

    //iniitialize the file descriptors
    int desc_count = 0;
    while(desc_count < processes_count) {
        //child's descriptors
        pipe(&child_file_descriptor[2*desc_count]);
        //parent's descriptors
        pipe(&parent_file_descriptor[2*desc_count]);
        desc_count++;
    }

    //spawn the child processes
    int i = 0;
    while (i < processes_count) {
        //if fork() returns error code like -1, then exit.
        if ((child_processes[i] = fork()) < 0) {
            perror("fork");
            exit(1);
        } else if (child_processes[i] == 0) {      //process spawned successfully
            pid_t pid = getpid();   //holds the current child process ID
            char mailbox[500];  //declare child buffer ... for holding incoming/outgoing messages

            strcpy(mailbox, intToString(pid));   //copy child ID into outgoing buffer
            printf("\n--------------------\n");
            printf("Child %d> ", pid);     //displays ID via output stream
            getCurrentTime();  //execute common function
            write(child_file_descriptor[2*i + 1], mailbox, 500);      //send message containing current child's ID to parent

            sleep(1);   //wait for child to write to parent ... transfers control to receiving parent

            read(parent_file_descriptor[2*i], mailbox, 500);     //child process fetches and stores reply from parent via parent's file descriptor
            printf("Child %d> INCOMING FROM PARENT: %s", pid, mailbox);     //child then displays message from parent
            printf("--------------------\n");

            exit(0);    //graceful exit

        } else {    //Parent zone
            char mailbox[500];  //declare parent buffer
            char fixed_msg[] = "Thank you child\n";

            read(child_file_descriptor[2*i], mailbox, 500);   //fetch and store message from child
            printf("++++++++++++++++++++\n");
            printf("Parent %d> My child %s just posted the time\n", parent_process, mailbox);
            printf("++++++++++++++++++++\n");
            write(parent_file_descriptor[2*i + 1], fixed_msg, 500);    //send message to child
        }
        sleep(1);   //allow time for each process to fetch system time by calling "getCurrentTime()"
        i++;
    }

    confirmExit(processes_count);    //confirm that all child processes terminated gracefully

    return 0;   //exit main function
}

//casts integer to string
char* intToString(int id) {
    char *text;
    text = malloc(sizeof(char) * 32);   //allocate size of previously declared character variable
    sprintf(text, "%d", id);    //cast integer parameter to string format
    return text;    //return newly formed string of characters
}

//function that fetches system time and displays it
void getCurrentTime() {
    time_t rawtime;     //data structure that holds time variables
    struct tm * timeinfo;

    time ( &rawtime );
    timeinfo = localtime ( &rawtime );
    printf ( "Current local time and date: %s", asctime (timeinfo) );
}

//wait for processes to terminate
void confirmExit(int processes_count) {
    int status;
    pid_t pid;
    while(processes_count > 0) {
        pid = wait(&status);
        printf("\nChild with PID %ld exited with status %d.\n", (long)pid, status);
        --processes_count;
    }
}